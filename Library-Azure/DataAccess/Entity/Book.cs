﻿using System.Collections.Generic;

namespace DataAccess.Entity
{
    public class Book : BaseEntity
    {
        public Book()
        {
            Authors = new List<Author>();
            Genres = new List<Genre>();
            UsersMarkedAsRead = new List<User>();
        }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string ImageUrl { get; set; }
        public int TimesRead { get; set; }

        public virtual List<Author> Authors { get; set; }
        public virtual List<Genre> Genres { get; set; }
        public virtual List<User> UsersMarkedAsRead { get; set; }

    }
}