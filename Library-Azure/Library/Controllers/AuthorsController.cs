﻿using DataAccess.Entity;
using DataAccess.Repository;
using Library.Models;
using Library.ViewModels.Authors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class AuthorsController : Controller
    {
        private AuthorRepository repo;
        public AuthorsController()
        {
            repo = new AuthorRepository();
        }


        public ActionResult Index()
        {
            List<Author> authors;
            authors = repo.GetAll();
            return View(authors);
        }

        public ActionResult Create()
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                AuthorViewModel model = new AuthorViewModel();
                return View(model);
            }
            return RedirectToAction("Index");

        }

        [HttpPost]
        public ActionResult Create(AuthorViewModel model)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                Author author = new Author();
                author.Name = model.Name;
                author.ImageUrl = model.ImageUrl;
                repo.Save(author);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                AuthorViewModel model = new AuthorViewModel();
                Author author = repo.GetById(id);

                if (author == null)
                {
                    return RedirectToAction("Index");
                }
                model.Id = author.Id;
                model.Name = author.Name;
                model.ImageUrl = author.ImageUrl;

                return View(model);
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Edit(AuthorViewModel model)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Author author = repo.GetById(model.Id);

                if (author == null)
                {
                    return HttpNotFound();
                }

                author.Id = model.Id;
                author.Name = model.Name;
                author.ImageUrl = model.ImageUrl;
                repo.Save(author);
            }
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                Author author = repo.GetById(id);
                if (author == null)
                {
                    return HttpNotFound();
                }
                repo.Delete(author);
            }
            return RedirectToAction("Index");
        }
    }
}