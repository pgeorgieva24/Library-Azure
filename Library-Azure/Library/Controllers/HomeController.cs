﻿using DataAccess.Entity;
using DataAccess.Repository;
using Library.Models;
using Library.ViewModels.Login;
using Library.ViewModels.Register;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            BookRepository repo = new BookRepository();
            List<Book> books = repo.GetAll().OrderByDescending(b => b.TimesRead).Take(3).ToList();
            return View(books);
        }

        [HttpGet]
        public ActionResult Login()
        {
            LoginViewModel model = new LoginViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            AuthenticationManager.Authenticate(model.Username, model.Password);

            if (AuthenticationManager.LoggedUser == null)
            {
                return View(model);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            if (AuthenticationManager.LoggedUser == null)
                return RedirectToAction("Login", "Home");

            AuthenticationManager.Logout();

            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {
            RegisterViewModel model = new RegisterViewModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            UserRepository repo = new UserRepository();

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (repo.IsUsernameFree(model.Username))
            {
                ViewData["UsernameTaken"] = "username is already taken";
                return View(model);
            }
            User user = new User();
            user.Id = model.Id;
            user.Username = model.Username;
            user.Password = model.Password;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;

            repo.Save(user);


            return RedirectToAction("Index", "Home");
        }
    }
}