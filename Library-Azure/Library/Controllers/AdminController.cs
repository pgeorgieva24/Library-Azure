﻿using DataAccess.Entity;
using DataAccess.Repository;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class AdminController : Controller
    {
        private UserRepository repo;
        public AdminController()
        {
            repo = new UserRepository();
        }

        public ActionResult Index()
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                List<User> users;
                users = repo.GetAll();
                User admin = repo.GetAll().Where(u => u.IsAdmin = true).FirstOrDefault();
                users.Remove(admin);
                return View(users);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult Edit(int id)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                User user = repo.GetById(id);
                if (user == null)
                {
                    return RedirectToAction("Index");
                }

                return View(user);

            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public ActionResult Edit(User model)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                User user = repo.GetById(model.Id);
                user.IsAdmin = model.IsAdmin;
                repo.Save(user);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult Delete(int id)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                User user = repo.GetById(id);
                repo.Delete(user);
                return RedirectToAction("Index");

            }
            return RedirectToAction("Login", "Home");
        }

    }
}