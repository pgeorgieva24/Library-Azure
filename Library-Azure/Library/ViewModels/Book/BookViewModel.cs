﻿
using DataAccess.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.ViewModels.Book
{
    public class BookViewModel
    {
        public BookViewModel()
        {
            SelectedAuthor = new List<int>();
            SelectedGenre = new List<int>();
            Authors = new List<Author>();
            Genres = new List<Genre>();
        }
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [DataType(DataType.MultilineText)]
        public string Summary { get; set; }
        public string ImageUrl { get; set; }
        public bool IsRead { get; set; }


        public List<int> SelectedAuthor { get; set; }
        public List<int> SelectedGenre { get; set; }
        public List<Author> Authors { get; set; }
        public List<Genre> Genres { get; set; }
    }
}