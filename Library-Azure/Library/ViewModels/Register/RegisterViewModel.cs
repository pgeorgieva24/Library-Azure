﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.ViewModels.Register
{
    public class RegisterViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [RegularExpression("[A-Za-z_ ]+", ErrorMessage = "letters only")]
        public string FirstName { get; set; }
        [RegularExpression("[A-Za-z_ ]+", ErrorMessage = "letters only")]
        public string LastName { get; set; }
    }
}